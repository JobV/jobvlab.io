---
title: My Photo Backup Setup
date: 2018-07-28 09:47 UTC
layout: article
---

# My Photo Backup Setup

## Goals

I now have a daughter, so want to make sure the pictures I take are not easily lost.

My goals:

- easily offload pictures from my camera to a place that is backed up
- maintain all originals
- backup local and in the cloud
- no need for moving to a local machine to edit pictures

## Setup

I hooked up a [Synology DS918+](https://www.synology.com/en-global/products/DS918)
to my main [network switch](https://www.ubnt.com/unifi-switching/unifi-switch-16-150w/).

The [drives](https://www.wdc.com/products/internal-storage/wd-red.html#WD20EFRX) in the
NAS run in RAID 1. I use a tool by Synology to automatically back up my photos folder to [Backblaze B2](https://www.backblaze.com/b2/cloud-storage.html), which is the easiest cold storage solution available.

## Workflow

Now I just offload my entire camera to the NAS directly from any computer in the house.
All photos are in RAW.

When I edit a photo, I save it using a standard format `yyyymmdd_originalfilename.tif`,
as to preserve a reference to the original, plus a date.

Edited photos are saved to another folder on the NAS. An automator script detects any change
to that folder and copies each new file to another folder, in a lower resolution and a
lossy file type. This way I immediately have small versions to share,
for minimal storage costs.